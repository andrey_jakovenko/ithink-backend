package application

import com.github.jasync.sql.db.SuspendingConnection
import com.github.jasync.sql.db.asSuspending
import com.github.jasync.sql.db.postgresql.PostgreSQLConnectionBuilder
import com.natpryce.konfig.Configuration
import configuration.ConfigKeys
import core.ModuleCompanion
import core.cqrs.command.CommandBus
import core.cqrs.event.EventBus
import core.cqrs.query.QueryBus
import core.router.CommandRouter
import core.transport.Mapper
import core.websocket.WebSocketHandler
import core.websocket.connections.Connections
import de.mkammerer.argon2.Argon2Factory
import org.koin.core.module.Module
import org.koin.dsl.module
import org.koin.experimental.builder.single

typealias Connection = SuspendingConnection

class AppModule(
        private val config: Configuration,
        private val modules: List<ModuleCompanion>) {

    private val baseModule = module(createdAtStart = true) {
        single { config }
        single { Mapper }
        single<Connections>()
        single<WebSocketHandler>()
        single<CommandRouter>()
        single { CommandBus() }
        single { QueryBus() }
        single { EventBus() }
        single {
            PostgreSQLConnectionBuilder.createConnectionPool {
                host = config[ConfigKeys.DB_HOST]
                port = config[ConfigKeys.DB_PORT]
                database = config[ConfigKeys.DB_DATABASE]
                username = config[ConfigKeys.DB_USERNAME]
                password = config[ConfigKeys.DB_PASSWORD]
            }.asSuspending
        }
        single {
            Argon2Factory.create(Argon2Factory.Argon2Types.ARGON2i)
        }
    }

    fun getAppModules(): List<ModuleCompanion> {
        return this.modules
    }

    fun getKoinModules(): List<Module> {
        return this.modules.map { it -> it.module } + this.baseModule
    }
}