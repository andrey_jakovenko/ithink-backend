package jashkasoft.ithink.application.auth

import am.ik.yavi.builder.ValidatorBuilder
import am.ik.yavi.builder.konstraint
import core.router.Context
import core.router.Handler
import core.router.Route
import core.router.send
import core.transport.Request
import core.websocket.ErrorType
import core.websocket.WebSocketError
import core.websocket.WebSocketResult
import core.websocket.connections.Connections
import core.websocket.connections.Identifier
import core.websocket.connections.Session
import de.mkammerer.argon2.Argon2
import io.ktor.features.origin
import java.time.LocalDateTime
import java.util.*

data class SignInRequest(
        val email: String,
        val password: String,
        val browser: String
)

@Route("signIn", SignInRequest::class)
class SignInHandler(
        private val connections: Connections,
        private val argon2: Argon2
): Handler<SignInRequest> {

    override suspend fun invoke(request: Request<SignInRequest>, ctx: Context) {
        val token = argon2.hash(
                8,
                65536,
                10,
                UUID.randomUUID().toString())
        val session = Session(
                ip = ctx.call.request.origin.remoteHost,
                browser = "Chrome",
                date = LocalDateTime.now().toString(),
                token = token
        )
        connections.add(Identifier(request.payload.email), session)

        ctx.send(
                WebSocketResult(
                        "signin",
                        token
                )
        )
    }

    override suspend fun validation(payload: SignInRequest, ctx: Context): Boolean {
        val validator = ValidatorBuilder.of<SignInRequest>()
                .konstraint(SignInRequest::browser) {
                    notNull()
                }
                .konstraint(SignInRequest::email) {
                    notNull()
                            .email()
                }
                .konstraint(SignInRequest::password) {
                    notNull()
                            .greaterThanOrEqual(8)
                }
                .build()
        val res = validator.validate(payload)

        if (res.isValid == false) {
            ctx.send(WebSocketError(
                    "signin.validation",
                    res.details(),
                    ErrorType.VALIDATION
            ))
        }

        return res.isValid
    }
}
