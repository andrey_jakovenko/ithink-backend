package application.auth

import core.ModuleCompanion
import core.router.Handler
import core.websocket.WebSocket
import core.websocket.WebSocketHandler
import core.websocket.WebSocketModule
import jashkasoft.ithink.application.auth.MeHandler
import jashkasoft.ithink.application.auth.MeMiddleware
import jashkasoft.ithink.application.auth.SignInHandler
import org.koin.core.KoinComponent
import org.koin.core.inject
import org.koin.dsl.module
import org.koin.experimental.builder.factory
import kotlin.reflect.KClass

class AuthModule: KoinComponent, WebSocketModule {
    override val path: String = "auth"
    private val handler: WebSocketHandler by inject()

    companion object: ModuleCompanion {
        override val module = module(createdAtStart = true) {
            factory<SignInHandler>()
            factory<MeHandler>()
            factory<MeMiddleware>()
        }

        override operator fun invoke(): AuthModule {
            return AuthModule()
        }
    }

    override fun getHandlers(): List<KClass<out Handler<out Any>>> {
        return listOf(
                MeHandler::class,
                SignInHandler::class
        )
    }

    override fun getWebSocketHandler(): WebSocket {
        return this.handler
    }
}