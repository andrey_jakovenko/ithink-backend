package jashkasoft.ithink.application.auth

import core.router.*
import core.transport.Request
import core.websocket.WebSocketResult
import core.websocket.connections.Connections

class Empty

class MeMiddleware(
        private val connections: Connections
): Middleware {
    override suspend fun invoke(
            handler: Handler<Any>,
            request: Request<Any>,
            ctx: Context) {
//        connections.hasSession()
        println(request.metaHeaders.get("reqId"))
    }
}

@Route("me", Empty::class)
class MeHandler: Handler<Empty> {

    override suspend fun invoke(request: Request<Empty>, ctx: Context) {
        ctx.send(
                WebSocketResult(
                        "me",
                        1
                )
        )
    }

    override fun middlewares(): Array<out Any> {
        return arrayOf(
                MeMiddleware::class
        )
    }
}
