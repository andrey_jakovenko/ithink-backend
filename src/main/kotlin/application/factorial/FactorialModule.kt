package application.factorial

import application.factorial.handlers.FactorialHandler
import application.factorial.handlers.FactorialQuery
import core.ModuleCompanion
import core.router.Handler
import core.websocket.WebSocket
import core.websocket.WebSocketHandler
import core.websocket.WebSocketModule
import org.koin.core.KoinComponent
import org.koin.core.inject
import org.koin.dsl.module
import org.koin.experimental.builder.factory
import org.koin.experimental.builder.single
import kotlin.reflect.KClass

class FactorialModule: KoinComponent, WebSocketModule {
    override val path: String = "factorial"
    private val handler: WebSocketHandler by inject()

    companion object: ModuleCompanion {
        override val module = module(createdAtStart = true) {
            factory<FactorialHandler>()
            factory<FactorialMiddleware>()
            single<FactorialQuery>()
        }

        override operator fun invoke(): FactorialModule {
            return FactorialModule()
        }
    }

    override fun getHandlers(): List<KClass<out Handler<out Any>>> {
        return listOf(
                FactorialHandler::class
        )
    }

    override fun getWebSocketHandler(): WebSocket {
        return this.handler
    }
}