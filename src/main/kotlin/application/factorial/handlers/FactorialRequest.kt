package application.factorial.handlers

data class FactorialRequest(val number: Int)