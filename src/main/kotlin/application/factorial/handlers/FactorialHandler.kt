package application.factorial.handlers

import application.Connection
import application.factorial.FactorialMiddleware
import core.cqrs.command.Command
import core.cqrs.query.QueryBus
import core.cqrs.query.QueryHandler
import core.router.Context
import core.router.Handler
import core.router.Route
import core.router.send
import core.transport.Request
import core.websocket.WebSocketResult
import java.math.BigInteger

data class FactorialCommand(val number: Int)

@QueryHandler(FactorialCommand::class)
class FactorialQuery: Command<FactorialCommand> {
    override suspend fun execute(command: FactorialCommand): BigInteger {
        var factorial = BigInteger.ONE
        for (i in 1..command.number) {
            factorial = factorial.multiply(BigInteger.valueOf(command.number.toLong()))
        }

        return factorial
    }
}

@Route("factorial", FactorialRequest::class)
class FactorialHandler(
        private val db: Connection,
        private val queryBus: QueryBus
): Handler<FactorialRequest> {

    override suspend fun invoke(request: Request<FactorialRequest>, ctx: Context) {
        val queryResult = db.sendPreparedStatement("select 1+?", listOf(request.payload.number))
        val sum = queryResult.rows[0].getAs<Int>(0)
        val result = queryBus.execute<Int>(FactorialCommand(number = request.payload.number))
        println("factorial result: $result, sum: $sum")

        ctx.send(
                WebSocketResult(
                        "factorial",
                        result
                )
        )
    }

    override fun middlewares(): Array<out Any> {
        return arrayOf(
                FactorialMiddleware::class
        )
    }
}
