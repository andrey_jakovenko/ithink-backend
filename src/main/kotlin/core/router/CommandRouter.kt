package core.router

import core.transport.Request
import core.websocket.ErrorType
import core.websocket.WebSocketError
import jashkasoft.ithink.core.websocket.MiddlewareException
import org.koin.core.KoinComponent
import kotlin.reflect.KClass
import kotlin.reflect.full.findAnnotation

open class CommandRouter : KoinComponent {
    private val handlers = mutableListOf<RouteCommand>()

    fun registerHandler(handler: KClass<out Handler<out Any>>) {
        val route = handler.findAnnotation<Route>()!!
        val command = RouteCommand(route.name, handler, route.dataClass)
        this.handlers.add(command)
    }

    fun findHandlerByCommand(command: String): RouteCommand? {
        return this.handlers.find { it.name == command }
    }

    suspend fun dispatch(command: RouteCommand, request: Request<Any>, ctx: Context) {
        val koin = this.getKoin()
        val handler = koin.get<Handler<Any>>(
                clazz = command.handler,
                qualifier = null,
                parameters = null
        )
        val middlewares = handler.middlewares()

        middlewares.forEach { it ->
            val middleware = koin.get<Middleware>(
                    clazz = it as KClass<*>,
                    qualifier = null,
                    parameters = null
            )
            try {
                middleware.invoke(handler, request, ctx)
            } catch (exception: MiddlewareException) {
                ctx.send(exception.error)
                return
            }
        }

        try {
            if (handler.validation(request.payload, ctx)) {
                handler.invoke(request, ctx)
            }
        } catch (exception: Exception) {
            ctx.send(WebSocketError(
                    "app.error",
                    exception.localizedMessage,
                    ErrorType.PARSE_REQUEST
            ))
        }
    }

}