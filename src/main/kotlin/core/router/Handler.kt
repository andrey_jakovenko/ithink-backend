package core.router

import core.transport.Request

interface Handler<T> {
    suspend fun invoke(request: Request<T>, ctx: Context)

    fun middlewares(): Array<out Any> {
        return arrayOf()
    }

    suspend fun validation(payload: T, ctx: Context): Boolean {
        return false
    }
}