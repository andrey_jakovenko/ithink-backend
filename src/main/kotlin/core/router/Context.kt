package core.router

import core.extensions.sendJSON
import core.websocket.WebSocketResponse
import io.ktor.application.ApplicationCall
import io.ktor.http.cio.websocket.Frame
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.channels.SendChannel

data class Context(val call: ApplicationCall,
                   val incoming: ReceiveChannel<Frame>,
                   val outgoing: SendChannel<Frame>)

suspend fun Context.send(json: WebSocketResponse) {
    this.outgoing.sendJSON(json)
}