package core.router

import core.transport.Request

interface Middleware {
    suspend fun invoke(handler: Handler<Any>,
                       request: Request<Any>,
                       ctx: Context)
}