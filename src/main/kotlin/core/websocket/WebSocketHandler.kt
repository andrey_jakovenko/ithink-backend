package core.websocket

import com.fasterxml.jackson.core.JsonParseException
import core.cqrs.event.EventBus
import core.router.CommandRouter
import core.router.Context
import core.router.RouteCommand
import core.router.send
import core.transport.Mapper
import core.transport.Request
import io.ktor.http.cio.websocket.CloseReason
import io.ktor.http.cio.websocket.Frame
import kotlinx.coroutines.Deferred

data class ClientConnected(val ctx: Context)
data class ClientConnectionError(val ctx: Context, val error: Error)
data class ClientCommandNotFound(val ctx: Context, val request: Request<Any>)
data class ClientDispatchCommand(val ctx: Context, val command: RouteCommand)
data class ClientRequestError(val ctx: Context, val error: JsonParseException)
data class ClientConnectionClose(val ctx: Context, val closeReason: Deferred<CloseReason?>)

class WebSocketHandler(private val router: CommandRouter,
                       private val mapper: Mapper,
                       private val eventBus: EventBus) : WebSocket {

    override suspend fun onConnect(ctx: Context) {
        eventBus.execute(ClientConnected(ctx))
    }

    override suspend fun onError(error: Error, ctx: Context) {
        println(error.localizedMessage)
        eventBus.execute(ClientConnectionError(ctx, error))
    }

    override suspend fun onMessage(frame: Frame.Binary, ctx: Context) {
        try {
            val length = frame.buffer.remaining()
            val byteArray = ByteArray(length)
            frame.buffer.get(byteArray)

            val request = this.mapper.mapTo<Request<Any>>(byteArray)
            val command = this.router.findHandlerByCommand(request.command)

            when (command) {
                null -> {
                    eventBus.execute(ClientCommandNotFound(ctx, request))
                    ctx.send(WebSocketError(
                            "app.commandNotFound",
                            "Route ${request.command} not found",
                            ErrorType.HANDLER_NOT_FOUND))
                }
                else -> {
                    eventBus.execute(ClientDispatchCommand(ctx, command))
                    this.router.dispatch(
                            command,
                            Request(
                                    request.command,
                                    this.mapper.mapTo(request.payload, command.dataClass),
                                    request.metaHeaders
                            ),
                            ctx
                    )
                }
            }
        } catch (exception: Exception) {
            this.handleException(exception, ctx)
        }
    }

    private suspend fun handleException(exception: Exception, ctx: Context) {
        when (exception) {
            is JsonParseException -> {
                println(exception.requestPayloadAsString)
                eventBus.execute(ClientRequestError(ctx, exception))
                ctx.send(WebSocketError(
                        "app.requestParsingError",
                        "Request contains invalid body",
                        ErrorType.PARSE_REQUEST))
            }
            is IllegalArgumentException -> {
                println(exception.message)
                ctx.send(WebSocketError(
                        "app.invalidSchema",
                        "JSON Invalid schema",
                        ErrorType.INVALID_JSON_SCHEMA))

            }
            else -> {
                ctx.send(WebSocketError(
                        "app.internal",
                        "Internal error",
                        ErrorType.APP_ERROR))
            }
        }
    }

    override suspend fun onClose(ctx: Context, closeReason: Deferred<CloseReason?>) {
        eventBus.execute(ClientConnectionClose(ctx, closeReason))
    }
}