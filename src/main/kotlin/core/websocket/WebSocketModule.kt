package core.websocket

import core.router.Handler
import kotlin.reflect.KClass

interface WebSocketModule {
    val path: String

    fun getWebSocketHandler(): WebSocket

    fun init() {}

    fun getHandlers(): List<KClass<out Handler<out Any>>>
}