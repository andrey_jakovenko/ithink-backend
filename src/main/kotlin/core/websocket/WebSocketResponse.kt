package core.websocket

enum class ErrorType(val code: Int) {
    APP_ERROR(100),
    VALIDATION(101),
    PARSE_REQUEST(102),
    HANDLER_NOT_FOUND(103),
    TRANSPORT_NOT_SUPPORTED(104),
    INVALID_JSON_SCHEMA(105)
}

sealed class WebSocketResponse
data class WebSocketResult(
        val channel: String,
        val payload: Any
) : WebSocketResponse()

data class WebSocketError(
        val channel: String,
        val payload: Any,
        val errorType: ErrorType
) : WebSocketResponse()