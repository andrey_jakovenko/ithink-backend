package jashkasoft.ithink.core.websocket

import core.websocket.WebSocketError

class MiddlewareException(val error: WebSocketError): Throwable()