package core.websocket.connections

data class Session(
        val token: String,
        val ip: String,
        val browser: String,
        val date: String
)
data class Identifier(val uuid: String)
data class UserMetadata(
        val sessions: List<Session> = listOf()
)

class Connections {
    private val connections = mutableMapOf<Identifier, UserMetadata>()

    fun add(id: Identifier, session: Session) {
        if (connections.contains(id)) {
            val connection = connections[id]

            connections[id] = UserMetadata(
                    sessions = connection!!.sessions + listOf(session)
            )

            return
        }

        connections[id] = UserMetadata(
                sessions = listOf(session)
        )
    }

    fun delete(id: Identifier) {
        this.connections.remove(id)
    }

//    fun hasSession(token: String): Boolean {
//    }

    fun list(): List<Pair<Identifier, UserMetadata>> {
        return this.connections.toList()
    }

    fun getById(id: Identifier): UserMetadata? {
        return this.connections[id]
    }

    fun removeSession(id: Identifier, token: String) {
        val connection = connections[id]

        if (connection === null) {
            return
        }

        connections[id] = UserMetadata(
                sessions = connection.sessions.filter { token != it.token }
        )
    }
}