package core

import core.websocket.WebSocketModule
import org.koin.core.module.Module

interface ModuleCompanion {
    val module: Module

    operator fun invoke(): WebSocketModule
}