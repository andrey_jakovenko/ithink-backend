package core.extensions

import core.router.Context
import core.router.send
import core.transport.Mapper
import core.websocket.ErrorType
import core.websocket.WebSocketError
import core.websocket.WebSocketModule
import core.websocket.WebSocketResponse
import io.ktor.http.cio.websocket.Frame
import io.ktor.routing.Route
import io.ktor.websocket.webSocket
import kotlinx.coroutines.channels.SendChannel
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.launch
import java.nio.ByteBuffer

fun Route.registerWSModule(module: WebSocketModule) {
    module.init()
    val handler = module.getWebSocketHandler()

    webSocket(module.path) {

        val context = Context(
                call,
                incoming,
                outgoing
        )

        handler.onConnect(context)

        try {
            incoming.consumeEach { frame ->
                when (frame) {
                    is Frame.Binary -> {
                        launch {
                            handler.onMessage(frame, context)
                        }
                    }
                    else -> {
                        context.send(
                                WebSocketError(
                                        "client.validation",
                                        "Frame ${frame::class} in not supported. Use only binary frame",
                                        ErrorType.TRANSPORT_NOT_SUPPORTED
                                )
                        )
                    }
                }
            }
        } catch (error: Error) {
            handler.onError(error, context)
        } finally {
            handler.onClose(context, closeReason)
        }
    }
}

suspend fun <E>SendChannel<E>.sendJSON(obj: WebSocketResponse) {
    val jsonStr = Mapper.toJSON(obj)
    val buffer = ByteBuffer.wrap( jsonStr.toByteArray() )
    val frame = Frame.Binary(
            fin = true,
            buffer = buffer
    )
    send( frame as E )
}