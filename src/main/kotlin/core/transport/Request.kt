package core.transport

class Request<T>(val command: String,
                 val payload: T,
                 val metaHeaders: MutableMap<String, Any> = mutableMapOf()
) {

    inline fun <reified T> getHeader(key: String, default: Any? = null): T {
        return this.metaHeaders.getOrDefault(key, default) as T
    }

    fun hasHeader(key: String): Boolean {
        return this.metaHeaders.containsKey(key)
    }
}