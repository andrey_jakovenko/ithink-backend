package core

import com.natpryce.konfig.Configuration
import configuration.ConfigKeys
import core.extensions.registerWSModule
import core.router.CommandRouter
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.html.respondHtml
import io.ktor.routing.get
import io.ktor.routing.routing
import io.ktor.server.cio.CIO
import io.ktor.server.engine.embeddedServer
import io.ktor.websocket.WebSockets
import kotlinx.html.head
import kotlinx.html.title
import org.koin.core.KoinComponent
import org.koin.core.inject

class Server(modules: List<ModuleCompanion>) : KoinComponent {
    private val config: Configuration by inject()
    private val router: CommandRouter by inject()
    private val server = embeddedServer(
            factory = CIO,
            port = config[ConfigKeys.SERVER_PORT],
            configure = {
                connectionIdleTimeoutSeconds = config[ConfigKeys.IDLE_TIMEOUT]
            }) {

        install(WebSockets)

        routing {

            get("/") {
                call.respondHtml {
                    head {
                        title { +"Example" }
                    }
                }
            }

            modules.forEach { it ->
                val module = it()
                module.getHandlers().forEach { handler -> router.registerHandler(handler) }
                registerWSModule(module)
            }
        }
    }

    fun start() {
        this.server.start(wait = true)
    }
}