package jashkasoft.ithink

import application.AppModule
import configuration.makeConfig
import configuration.modules
import core.Server
import org.koin.core.context.startKoin

fun main(args: Array<String>) {
    val config = makeConfig()

    val definition = AppModule(config, modules)

    startKoin {
        modules(definition.getKoinModules())
    }

    Server(definition.getAppModules()).start()
}