package configuration

import com.natpryce.konfig.Key
import com.natpryce.konfig.intType
import com.natpryce.konfig.stringType

class ConfigKeys {
    companion object {
        val SERVER_PORT = Key("server.port", intType)
        val SERVER_HOST = Key("server.host", intType)
        val IDLE_TIMEOUT = Key("idle_timeout", intType)

        val DB_HOST = Key("db.host", stringType)
        val DB_PORT = Key("db.port", intType)
        val DB_DATABASE = Key("db.database", stringType)
        val DB_USERNAME = Key("db.username", stringType)
        val DB_PASSWORD = Key("db.password", stringType)

        val COOKIE_NAME = Key("cookie_name", stringType)
    }
}