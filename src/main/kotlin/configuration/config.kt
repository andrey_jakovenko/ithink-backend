package configuration

import application.auth.AuthModule
import application.factorial.FactorialModule
import com.natpryce.konfig.Configuration
import com.natpryce.konfig.ConfigurationProperties
import com.natpryce.konfig.EnvironmentVariables
import com.natpryce.konfig.overriding

fun makeConfig(path: String = "local.properties"): Configuration {
    return ConfigurationProperties.systemProperties() overriding
            EnvironmentVariables() overriding
            ConfigurationProperties.fromResource(path)

}

val modules = listOf(
        FactorialModule,
        AuthModule
)