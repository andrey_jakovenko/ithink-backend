package test

import application.factorial.handlers.Factorial
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.math.BigInteger

class FactorialTest {
    @Test
    fun `factorial computation`() {
        val factorial = Factorial()
        assertEquals(factorial.compute(2), BigInteger.valueOf(4))
    }

    @Test
    fun `factorial computation by one`() {
        val factorial = Factorial()
        assertEquals(factorial.compute(1), BigInteger.ONE)
    }
}